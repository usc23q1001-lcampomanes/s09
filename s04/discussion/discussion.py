# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in following format:
# The grade of John is 100
students = ["Jennie", "Jisoo", "Rose", "Lisa", "Mingyu"]
grades = [70, 80, 90, 100, 60]
i = 0
while i < 5:
	print(f"The grade of {students[i]} is {grades[i]}")
	i+=1


# Mini Exercise 3:30
# 1. Create a car dictionary with the following keys
# brand, model, year of make, color
# 2. Print the following statement from the details:
# "I own a <brand> <model> and it was made in <year of make>"

car = {"brand": "Mercedes", "model": "E-Class", "year_of_make": 2023, "color": "silver"}
print(f"I own a {car['brand']} {car['model']}, and it was made in {car['year_of_make']}")